/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   talker.h
 * Author: vandenberghe
 *
 * Created on 4 avril 2016, 16:07
 */

#ifndef TALKER_H
#define TALKER_H
#include "ros/ros.h"

class Talker {
private:
    ros::Publisher _chatter_pub;
    ros::Rate _loop_rate;
    ros::NodeHandle _n;
    Talker(int argc, char **argv,int rate_max, int buffer_size);
    static Talker* _instance;
public:
    static Talker getInstance();
};


#endif /* TALKER_H */


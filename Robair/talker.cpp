/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include "talker.h"
#include "std_msgs/String.h"

#include <sstream>
using namespace ros;
using namespace std;

Talker::Talker(int argc, char **argv,int rate_max, int buffer_size)
:_loop_rate(rate_max){
    init(argc, argv, "suivis");
    _chatter_pub = _n.advertise<std_msgs::String>("cmdmotors", buffer_size);
    
}

Talker* Talker::getInstance() {
    if (_instance == NULL) {
        _instance = new Talker(10);
    }
    return _instance;
}


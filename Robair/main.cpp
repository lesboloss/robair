/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: vandenberghe
 *
 * Created on 4 avril 2016, 16:06
 */

#include <cstdlib>
#include "ros/ros.h"
#include "std_msgs/String.h"

#include "talker.cpp"

#include <sstream>
using namespace std;

int main(int argc, char **argv) {
    //Init de ROS
    ros::init(argc, argv, "suivis");
    //Init du noeud et d'un handler
    ros::NodeHandle n;
    //Init que l'on va publier sur le topic (taille buffer)
    ros::Publisher chatter_pub = n.advertise<std_msgs::String>("cmdmotors", 1000);
    //Init fréquence
    ros::Rate loop_rate(10);
    int count = 0;
    while (ros::ok()) {
        //Contenu du message
        std_msgs::String msg;
        std::stringstream ss;
        ss << "10 -10";
        msg.data = ss.str();
        ROS_INFO("%s", msg.data.c_str());
        //publication
        chatter_pub.publish(msg);
        ros::spinOnce();
        loop_rate.sleep();
        ++count;
    }
    return 0;
}


